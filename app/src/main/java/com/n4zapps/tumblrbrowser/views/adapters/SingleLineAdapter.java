package com.n4zapps.tumblrbrowser.views.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.n4zapps.tumblrbrowser.R;
import com.n4zapps.tumblrbrowser.interfaces.ShowPostsActivityMVP;
import com.n4zapps.tumblrbrowser.views.viewholders.SingleLineViewHolder;

public class SingleLineAdapter extends RecyclerView.Adapter<SingleLineViewHolder> {

    private ShowPostsActivityMVP.SingleLinePresenter presenter;

    public SingleLineAdapter(ShowPostsActivityMVP.SingleLinePresenter presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public SingleLineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SingleLineViewHolder(LayoutInflater.from(parent.getContext())
                                    .inflate(R.layout.cell_single_line, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SingleLineViewHolder holder, int position) {
        presenter.onBindSingleLineViewAtPosition(position, holder);
    }

    @Override
    public int getItemCount() {
        return presenter.getItemCount();
    }

}
