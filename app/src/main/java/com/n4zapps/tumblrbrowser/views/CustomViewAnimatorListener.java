package com.n4zapps.tumblrbrowser.views;

import android.animation.Animator;

public abstract class CustomViewAnimatorListener implements Animator.AnimatorListener {

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }

}
