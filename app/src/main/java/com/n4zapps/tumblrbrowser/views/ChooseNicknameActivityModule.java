package com.n4zapps.tumblrbrowser.views;

import android.content.Context;

import com.n4zapps.tumblrbrowser.interfaces.ChooseNicknameActivityMVP;
import com.n4zapps.tumblrbrowser.interfaces.PostRepository;
import com.n4zapps.tumblrbrowser.model.ChooseNicknameModel;
import com.n4zapps.tumblrbrowser.presenters.ChooseNicknamePresenter;
import com.n4zapps.tumblrbrowser.utils.requests.RequestManager;

import dagger.Module;
import dagger.Provides;

@Module
public class ChooseNicknameActivityModule {

    @Provides
    ChooseNicknameActivityMVP.View provideChooseNicknameView(ChooseNicknameActivity activity) {
        return activity;
    }

    @Provides
    ChooseNicknameActivityMVP.Presenter provideChooseNicknamePresenter(ChooseNicknameActivityMVP.View view, ChooseNicknameActivityMVP.Model model) {
        return new ChooseNicknamePresenter(view, model);
    }

    @Provides
    ChooseNicknameActivityMVP.Model provideChooseNicknameModel(RequestManager requestManager, PostRepository postRepository) {
        return new ChooseNicknameModel(requestManager, postRepository);
    }

    @Provides
    RequestManager provideRequestManager(Context context) {
        return new RequestManager(context);
    }

}
