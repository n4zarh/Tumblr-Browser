package com.n4zapps.tumblrbrowser.views.viewholders;

import com.n4zapps.tumblrbrowser.interfaces.PostRepository;
import com.n4zapps.tumblrbrowser.interfaces.ShowPostsActivityMVP;
import com.n4zapps.tumblrbrowser.model.SingleLineModel;
import com.n4zapps.tumblrbrowser.presenters.SingleLinePresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ConversationViewHolderModule {

    @Provides
    ShowPostsActivityMVP.SingleLineModel provideSingleLineModel(PostRepository postRepository) {
        return new SingleLineModel(postRepository);
    }

    @Provides
    ShowPostsActivityMVP.SingleLinePresenter provideSingleLinePresenter(ShowPostsActivityMVP.SingleLineModel model) {
        return new SingleLinePresenter(model);
    }

}
