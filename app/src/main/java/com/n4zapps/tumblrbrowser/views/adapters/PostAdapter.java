package com.n4zapps.tumblrbrowser.views.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.n4zapps.tumblrbrowser.R;
import com.n4zapps.tumblrbrowser.interfaces.ShowPostsActivityMVP;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.BasePost;
import com.n4zapps.tumblrbrowser.utils.Constants;
import com.n4zapps.tumblrbrowser.utils.Logger;
import com.n4zapps.tumblrbrowser.views.viewholders.BaseViewHolder;
import com.n4zapps.tumblrbrowser.views.viewholders.ConversationViewHolder;
import com.n4zapps.tumblrbrowser.views.viewholders.PhotoViewHolder;
import com.n4zapps.tumblrbrowser.views.viewholders.PostViewHolder;

@SuppressWarnings("ConstantConditions")
public class PostAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int ITEM_VIEW_TYPE_REGULAR      = 0,
                            ITEM_VIEW_TYPE_PHOTO        = 6,
                            ITEM_VIEW_TYPE_CONVERSATION = 7;

    private ShowPostsActivityMVP.Presenter presenter;

    public PostAdapter(ShowPostsActivityMVP.Presenter presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_VIEW_TYPE_REGULAR:
                return new PostViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.cell_post, parent, false));
            case ITEM_VIEW_TYPE_PHOTO:
                return new PhotoViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.cell_photo_post, parent, false));
            case ITEM_VIEW_TYPE_CONVERSATION:
                return new ConversationViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.cell_conversation_post, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        Logger.d("BINDING", String.valueOf(position));
        switch (getItemViewType(position)) {
            case ITEM_VIEW_TYPE_REGULAR:
                presenter.onBindRegularCellViewAtPosition(position, (PostViewHolder)holder);
                break;
            case ITEM_VIEW_TYPE_PHOTO:
                presenter.onBindPhotoCellViewAtPosition(position, (PhotoViewHolder)holder);
                break;
            case ITEM_VIEW_TYPE_CONVERSATION:
                presenter.onBindConversationViewAtPosition(position, (ConversationViewHolder)holder);
                break;
        }
        //presenter.onBindRepositoryCellViewAtPosition(position, holder);
        if (position % Constants.NUMBER_OF_POSTS_PER_QUERY == Constants.POST_MOD_TO_DOWNLOAD_NEW_DATA) {
            presenter.downloadNextPage();
        }
    }

    @Override
    public int getItemCount() {
        return presenter.getItemCount();
    }

    @Override
    public int getItemViewType(int position) {
        String type = presenter.getItemType(position);
        switch (type) {

            case BasePost.TYPE_ANSWER:
            case BasePost.TYPE_AUDIO:
            case BasePost.TYPE_LINK:
            case BasePost.TYPE_QUOTE:
            case BasePost.TYPE_REGULAR:
            case BasePost.TYPE_TEXT:
            case BasePost.TYPE_VIDEO:
                return ITEM_VIEW_TYPE_REGULAR;

            case BasePost.TYPE_PHOTO:
                return ITEM_VIEW_TYPE_PHOTO;

            case BasePost.TYPE_CHAT:
            case BasePost.TYPE_CONVERSATION:
                return ITEM_VIEW_TYPE_CONVERSATION;

        }
        return super.getItemViewType(position);
    }

}
