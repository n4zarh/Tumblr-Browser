package com.n4zapps.tumblrbrowser.views;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.n4zapps.tumblrbrowser.R;
import com.n4zapps.tumblrbrowser.interfaces.ShowPostsActivityMVP;
import com.n4zapps.tumblrbrowser.views.adapters.PostAdapter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;

public class ShowPostsActivity extends AppCompatActivity
                                implements ShowPostsActivityMVP.View {

    public static final String EXTRA_PARAM_USERNAME     = "USERNAME",
                                EXTRA_PARAM_TYPE        = "TYPE";

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.titleTextView)
    TextView titleTextView;
    @BindView(R.id.loadingProgress)
    ProgressBar loadingProgress;
    private PostAdapter adapter;

    @Inject ShowPostsActivityMVP.Presenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_posts);
        ButterKnife.bind(this);
        String username = getIntent().getStringExtra(EXTRA_PARAM_USERNAME);
        String type = getIntent().getStringExtra(EXTRA_PARAM_TYPE);
        titleTextView.setText(String.format("%s%s", username, getString(R.string.show_feed_title_suffix)));
        presenter.setUsername(username);
        presenter.setType(type);
        adapter = new PostAdapter(presenter);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                Uri uri = Uri.parse(presenter.getItemUri(position));
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        }));
    }

    @UiThread
    @Override
    public void notifyAdapterAboutNewItems(int firstAddedAtIndex, int numberOfItems) {
        adapter.notifyItemRangeInserted(firstAddedAtIndex, numberOfItems);
    }

    @Override
    public String getStringFromResources(int resId) {
        return getString(resId);
    }

}
