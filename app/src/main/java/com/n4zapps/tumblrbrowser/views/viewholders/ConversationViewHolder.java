package com.n4zapps.tumblrbrowser.views.viewholders;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.n4zapps.tumblrbrowser.R;
import com.n4zapps.tumblrbrowser.interfaces.ConversationViewHolderComponent;
import com.n4zapps.tumblrbrowser.interfaces.DaggerConversationViewHolderComponent;
import com.n4zapps.tumblrbrowser.interfaces.FeedCellView;
import com.n4zapps.tumblrbrowser.interfaces.ShowPostsActivityMVP;
import com.n4zapps.tumblrbrowser.root.MyApplication;
import com.n4zapps.tumblrbrowser.views.adapters.SingleLineAdapter;

import javax.inject.Inject;

import butterknife.BindView;

public class ConversationViewHolder extends BaseViewHolder implements FeedCellView.ConversationCellView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private SingleLineAdapter adapter;
    @Inject ShowPostsActivityMVP.SingleLinePresenter presenter;
    private static ConversationViewHolderComponent component;

    public ConversationViewHolder(View itemView) {
        super(itemView);
        component = DaggerConversationViewHolderComponent.builder()
                .applicationComponent(MyApplication.getComponent())
                .repositoryModule(MyApplication.getRepositoryModule())
                .conversationViewHolderModule(new ConversationViewHolderModule())
                .build();
        component.inject(this);
        adapter = new SingleLineAdapter(presenter);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
    }

    @Override
    public void setConversationPresenter(int position) {
        presenter.setNewPosition(position);
    }

}
