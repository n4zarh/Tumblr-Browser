package com.n4zapps.tumblrbrowser.views;

import android.content.Context;

import com.n4zapps.tumblrbrowser.interfaces.PostRepository;
import com.n4zapps.tumblrbrowser.interfaces.ShowPostsActivityMVP;
import com.n4zapps.tumblrbrowser.model.PostsModel;
import com.n4zapps.tumblrbrowser.presenters.ShowPostsPresenter;
import com.n4zapps.tumblrbrowser.utils.requests.RequestManager;

import dagger.Module;
import dagger.Provides;

@Module
public class ShowPostsActivityModule {

    @Provides
    ShowPostsActivityMVP.View provideShowPostsView(ShowPostsActivity view) {
        return view;
    }

    @Provides
    ShowPostsActivityMVP.Presenter provideShowPostsPresenter(ShowPostsActivityMVP.View view, ShowPostsActivityMVP.Model model) {
        return new ShowPostsPresenter(view, model);
    }

    @Provides
    ShowPostsActivityMVP.Model provideShowPostsModel(RequestManager requestManager, PostRepository postRepository) {
        return new PostsModel(requestManager, postRepository);
    }

    @Provides
    RequestManager provideRequestManager(Context context) {
        return new RequestManager(context);
    }

}
