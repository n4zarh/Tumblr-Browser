package com.n4zapps.tumblrbrowser.views.viewholders;

import android.view.View;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.n4zapps.tumblrbrowser.R;
import com.n4zapps.tumblrbrowser.interfaces.FeedCellView;

import butterknife.BindView;

public class PhotoViewHolder extends BaseViewHolder implements FeedCellView.PhotoCellView {

    @BindView(R.id.imageView)
    NetworkImageView imageView;

    public PhotoViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setPhoto(String url, ImageLoader imageLoader) {
        imageView.setImageUrl(url, imageLoader);
    }

}
