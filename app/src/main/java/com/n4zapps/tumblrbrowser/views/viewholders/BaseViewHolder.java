package com.n4zapps.tumblrbrowser.views.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.n4zapps.tumblrbrowser.R;
import com.n4zapps.tumblrbrowser.interfaces.FeedCellView;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseViewHolder extends RecyclerView.ViewHolder
                                        implements FeedCellView {

    @BindView(R.id.dateTimeTextView)
    TextView dateTimeTextView;
    @BindView(R.id.typeTextView)
    TextView typeTextView;
    @BindView(R.id.titleTextView)
    TextView titleTextView;

    BaseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setNameDateAndType(String name, String date, String type) {
        if (name == null || name.isEmpty()) {
            titleTextView.setVisibility(View.GONE);
        } else {
            titleTextView.setVisibility(View.VISIBLE);
            titleTextView.setText(name);
        }
        dateTimeTextView.setText(date);
        String uppedType = type.substring(0, 1).toUpperCase() + type.substring(1);
        typeTextView.setText(uppedType);
    }

}
