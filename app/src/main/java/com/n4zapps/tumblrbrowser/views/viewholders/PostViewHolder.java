package com.n4zapps.tumblrbrowser.views.viewholders;

import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.n4zapps.tumblrbrowser.R;
import com.n4zapps.tumblrbrowser.interfaces.FeedCellView;

import butterknife.BindView;

public class PostViewHolder extends BaseViewHolder implements FeedCellView.RegularCellView {

    @BindView(R.id.detailsTextView)
    TextView detailsTextView;

    public PostViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setDetails(String details) {
        if (details == null || details.isEmpty()) {
            detailsTextView.setVisibility(View.GONE);
        } else {
            detailsTextView.setVisibility(View.VISIBLE);
            detailsTextView.setText(details);
        }
    }

    @Override
    public void setDetailsGravityToRight() {
        detailsTextView.setGravity(Gravity.RIGHT);
    }

}
