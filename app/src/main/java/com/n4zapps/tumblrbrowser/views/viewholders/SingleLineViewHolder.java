package com.n4zapps.tumblrbrowser.views.viewholders;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.n4zapps.tumblrbrowser.R;
import com.n4zapps.tumblrbrowser.interfaces.SingleLineCellView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SingleLineViewHolder extends RecyclerView.ViewHolder implements SingleLineCellView {

    @BindView(R.id.parentLayout)
    LinearLayout parentLayout;
    @BindView(R.id.nameTextView)
    TextView nameTextView;
    @BindView(R.id.messageTextView)
    TextView messageTextView;

    public SingleLineViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setTextLine(String name, String message, boolean setGrayBackground) {
        nameTextView.setText(name + ": ");
        messageTextView.setText(message);
        parentLayout.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), setGrayBackground ? R.color.gray : R.color.lightGray));
    }

}
