package com.n4zapps.tumblrbrowser.views;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.n4zapps.tumblrbrowser.R;
import com.n4zapps.tumblrbrowser.interfaces.ChooseNicknameActivityMVP;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;

public class ChooseNicknameActivity extends AppCompatActivity
                                    implements ChooseNicknameActivityMVP.View {

    @BindView(R.id.selectNameButton)
    Button button;
    @BindView(R.id.edittext)
    TextInputEditText editText;
    @BindView(R.id.advancedOptionsButton)
    Button advancedOptionsButton;
    @BindView(R.id.advancedOptionsLayout)
    LinearLayout advancedOptionsLayout;
    @BindView(R.id.typeRadioGroup)
    RadioGroup typeRadioGroup;
    @Inject ChooseNicknameActivityMVP.Presenter presenter;
    private boolean showAdvancedOptions = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
        setContentView(R.layout.activity_choose_nickname);
        ButterKnife.bind(this);
    }

    @Override
    public String getUsername() {
        if (editText.getText() != null && editText.getText().length() > 0) {
            return editText.getText().toString();
        }
        return null;
    }

    @Override
    public void showNoUsernameErrorToast() {
        showToast(R.string.error_no_username_provided);
    }

    @Override
    public void showNoUserFoundErrorToast() {
        showToast(R.string.error_no_user_found);
    }

    @Override
    public void moveToShowDataActivity(String username, String type) {
        Intent intent = new Intent(getApplicationContext(), ShowPostsActivity.class);
        if (type != null) {
            intent.putExtra(ShowPostsActivity.EXTRA_PARAM_TYPE, type);
        }
        intent.putExtra(ShowPostsActivity.EXTRA_PARAM_USERNAME, username);
        startActivity(intent);
    }

    @Override
    public void showNoConnectionErrorToast() {
        showToast(R.string.error_no_connection);
    }

    @Override
    public void showEmptyFeedError() {
        showToast(R.string.error_empty_feed);
    }

    @Override
    public int getCheckedTypeId() {
        return typeRadioGroup.getCheckedRadioButtonId();
    }

    private void showToast(int stringId) {
        Toast.makeText(this, getString(stringId), Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.selectNameButton)
    public void onButtonClick() {
        presenter.onGetDataButtonClicked();
    }

    @OnClick(R.id.advancedOptionsButton)
    public void onAdvancedOptionsButtonClick() {
        showAdvancedOptions = !showAdvancedOptions;
        advancedOptionsLayout.animate().alpha(showAdvancedOptions ? 1.0f : 0.0f).setDuration(500).setListener(new CustomViewAnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                advancedOptionsButton.setEnabled(false);
                if (showAdvancedOptions) {
                    advancedOptionsLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                advancedOptionsButton.setEnabled(true);
                if (!showAdvancedOptions) {
                    advancedOptionsLayout.setVisibility(View.GONE);
                }
            }
        });
    }

}
