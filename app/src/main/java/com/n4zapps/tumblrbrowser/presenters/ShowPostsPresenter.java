package com.n4zapps.tumblrbrowser.presenters;

import com.n4zapps.tumblrbrowser.R;
import com.n4zapps.tumblrbrowser.interfaces.ShowPostsActivityMVP;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.BasePost;
import com.n4zapps.tumblrbrowser.utils.Constants;
import com.n4zapps.tumblrbrowser.views.viewholders.BaseViewHolder;
import com.n4zapps.tumblrbrowser.views.viewholders.ConversationViewHolder;
import com.n4zapps.tumblrbrowser.views.viewholders.PhotoViewHolder;
import com.n4zapps.tumblrbrowser.views.viewholders.PostViewHolder;

import javax.inject.Inject;

public class ShowPostsPresenter implements ShowPostsActivityMVP.Presenter {

    private ShowPostsActivityMVP.View view;
    private ShowPostsActivityMVP.Model model;
    private int page = 0;
    private String username;
    private String type;

    @Inject
    public ShowPostsPresenter(ShowPostsActivityMVP.View view, ShowPostsActivityMVP.Model model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void downloadNextPage() {
        if (model.getItemCount() % Constants.NUMBER_OF_POSTS_PER_QUERY == 0) { //If there's different value we know that those are last posts in this feed
            model.downloadData(username, type, ++page, onDataDownloadedListener);
        }
    }

    @Override
    public String getItemType(int position) {
        return model.getItemType(position);
    }

    @Override
    public void onBindRegularCellViewAtPosition(int position, PostViewHolder viewHolder) {
        bindBase(position, viewHolder);
        String details;
        if (model.getItemType(position).equals(BasePost.TYPE_AUDIO)) {
            details = view.getStringFromResources(R.string.go_to_audio);
            viewHolder.setDetailsGravityToRight();
        } else if (model.getItemType(position).equals(BasePost.TYPE_VIDEO)) {
            details = view.getStringFromResources(R.string.go_to_video);
            viewHolder.setDetailsGravityToRight();
        } else {
            details = model.getItemDetails(position);
            if (model.getItemType(position).equals(BasePost.TYPE_QUOTE)) {
                viewHolder.setDetailsGravityToRight();
            }
        }
        viewHolder.setDetails(details);
    }

    @Override
    public void onBindPhotoCellViewAtPosition(int position, PhotoViewHolder viewHolder) {
        bindBase(position, viewHolder);
        viewHolder.setPhoto(model.getPhotoUrl(position), model.getImageLoader());
    }

    @Override
    public void onBindConversationViewAtPosition(int position, ConversationViewHolder viewHolder) {
        bindBase(position, viewHolder);
        viewHolder.setConversationPresenter(position);
    }

    @Override
    public String getItemUri(int position) {
        return model.getItemUri(position);
    }

    @Override
    public int getItemCount() {
        return model.getItemCount();
    }

    private void bindBase(int position, BaseViewHolder viewHolder) {
        viewHolder.setNameDateAndType(model.getItemName(position),
                                        model.getItemDate(position),
                                        model.getItemType(position));
    }

    private ShowPostsActivityMVP.Model.OnDataDownloadedListener onDataDownloadedListener = new ShowPostsActivityMVP.Model.OnDataDownloadedListener() {
        @Override
        public void onDataLoaded(int count) {
            view.notifyAdapterAboutNewItems(page * Constants.NUMBER_OF_POSTS_PER_QUERY, count);
        }
    };

}
