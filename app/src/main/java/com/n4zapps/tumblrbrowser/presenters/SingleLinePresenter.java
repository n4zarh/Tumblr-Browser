package com.n4zapps.tumblrbrowser.presenters;

import com.n4zapps.tumblrbrowser.interfaces.ShowPostsActivityMVP;
import com.n4zapps.tumblrbrowser.views.viewholders.SingleLineViewHolder;

import javax.inject.Inject;

public class SingleLinePresenter implements ShowPostsActivityMVP.SingleLinePresenter {

    private int itemPosition;
    private ShowPostsActivityMVP.SingleLineModel model;

    @Inject
    public SingleLinePresenter(ShowPostsActivityMVP.SingleLineModel model) {
        this.model = model;
    }

    @Override
    public void onBindSingleLineViewAtPosition(int position, SingleLineViewHolder viewHolder) {
        viewHolder.setTextLine(model.getConversation(itemPosition).get(position).getName(),
                                model.getConversation(itemPosition).get(position).getPhrase(),
                                position % 2 == 1);
    }

    @Override
    public void setNewPosition(int position) {
        this.itemPosition = position;
    }

    @Override
    public int getItemCount() {
        return model.getConversation(itemPosition).size();
    }

}
