package com.n4zapps.tumblrbrowser.presenters;

import com.n4zapps.tumblrbrowser.R;
import com.n4zapps.tumblrbrowser.interfaces.ChooseNicknameActivityMVP;
import com.n4zapps.tumblrbrowser.model.ChooseNicknameModel;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.BasePost;

import javax.inject.Inject;

public class ChooseNicknamePresenter implements ChooseNicknameActivityMVP.Presenter {

    private ChooseNicknameActivityMVP.View view;
    private ChooseNicknameActivityMVP.Model model;
    private String username;
    private String type;

    @Inject
    public ChooseNicknamePresenter(ChooseNicknameActivityMVP.View view,
                                   ChooseNicknameActivityMVP.Model model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void onGetDataButtonClicked() {
        username = view.getUsername();
        int typeId = view.getCheckedTypeId();
        switch (typeId) {
            case R.id.audioRadio:
                type = BasePost.TYPE_AUDIO;
                break;
            case R.id.chatRadio:
                type = BasePost.TYPE_CHAT;
                break;
            case R.id.linkRadio:
                type = BasePost.TYPE_LINK;
                break;
            case R.id.photoRadio:
                type = BasePost.TYPE_PHOTO;
                break;
            case R.id.quoteRadio:
                type = BasePost.TYPE_QUOTE;
                break;
            case R.id.textRadio:
                type = BasePost.TYPE_TEXT;
                break;
            case R.id.videoRadio:
                type = BasePost.TYPE_VIDEO;
                break;
            case R.id.allTypesRadio:
            default:
                type = null;
                break;
        }
        if (username == null) {
            view.showNoUsernameErrorToast();
        } else {
            model.downloadData(username, type, onDataDownloadedListener);
        }
    }

    private ChooseNicknameModel.OnDataDownloadedListener onDataDownloadedListener = new ChooseNicknameActivityMVP.Model.OnDataDownloadedListener() {
        @Override
        public void onDataLoaded(int count) {
            view.moveToShowDataActivity(username, type);
        }

        @Override
        public void usernameNotFound() {
            view.showNoUserFoundErrorToast();
        }

        @Override
        public void noConnectionError() {
            view.showNoConnectionErrorToast();
        }

        @Override
        public void emptyFeed() {
            view.showEmptyFeedError();
        }
    };

}
