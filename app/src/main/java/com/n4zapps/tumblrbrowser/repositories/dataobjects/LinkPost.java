package com.n4zapps.tumblrbrowser.repositories.dataobjects;

import java.util.List;

public class LinkPost extends BasePost {

    private String linkText;
    private String linkUrl;

    public LinkPost(String id, String url, long timestamp, String type, List<String> tags, String linkText, String linkUrl) {
        super(id, url, timestamp, type, tags);
        this.linkText = linkText;
        this.linkUrl = linkUrl;
    }

    public String getLinkText() {
        return linkText;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

}
