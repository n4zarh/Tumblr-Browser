package com.n4zapps.tumblrbrowser.repositories;

import com.n4zapps.tumblrbrowser.interfaces.PostRepository;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.AnswerPost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.AudioPost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.BasePost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.ConversationPost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.LinkPost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.PhotoPost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.QuotePost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.RegularPost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.VideoPost;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class MemoryPostRepository implements PostRepository {

    private List<BasePost> posts;

    public MemoryPostRepository() {
        posts = new ArrayList<>();
    }

    @Override
    public void populateList(BasePost[] objects) {
        posts.addAll(Arrays.asList(objects));
    }

    @Override
    public void clearList() {
        posts.clear();
    }

    @Override
    public int getPostCount() {
        return posts.size();
    }

    @Override
    public String getPostType(int index) {
        return posts.get(index).getType();
    }

    @Override
    public String getPostText(int index) {
        BasePost post = posts.get(index);
        switch (post.getType()) {
            case BasePost.TYPE_ANSWER:
                AnswerPost answerPost = (AnswerPost)post;
                return answerPost.getQuestion();
            case BasePost.TYPE_AUDIO:
                AudioPost audioPost = (AudioPost)post;
                return audioPost.getAudioCaption();
            case BasePost.TYPE_CHAT:
            case BasePost.TYPE_CONVERSATION:
                ConversationPost conversationPost = (ConversationPost)post;
                return conversationPost.getConversationTitle();
            case BasePost.TYPE_LINK:
                LinkPost linkPost = (LinkPost)post;
                return linkPost.getLinkText();
            case BasePost.TYPE_PHOTO:
                PhotoPost photoPost = (PhotoPost)post;
                return photoPost.getPhotoCaption();
            case BasePost.TYPE_QUOTE:
                QuotePost quotePost = (QuotePost)post;
                return quotePost.getQuoteText();
            case BasePost.TYPE_REGULAR:
            case BasePost.TYPE_TEXT:
                RegularPost regularPost = (RegularPost)post;
                return regularPost.getRegularTitle();
            case BasePost.TYPE_VIDEO:
                VideoPost videoPost = (VideoPost)post;
                return videoPost.getVideoCaption();
        }
        return null;
    }

    @Override
    public String getPostTime(int index) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(posts.get(index).getTimestamp() * 1000);
        return new SimpleDateFormat("dd MMM yyyy, HH:mm").format(calendar.getTime());
    }

    @Override
    public String getPostUri(int index) {
        return posts.get(index).getUrl();
    }

    @Override
    public String getAnswerPostDetails(int index) {
        return ((AnswerPost)posts.get(index)).getAnswer();
    }

    @Override
    public String getLinkPostDetails(int index) {
        return ((LinkPost)posts.get(index)).getLinkUrl();
    }

    @Override
    public String getPhotoPostLink(int index, int maxWidth) {
        return ((PhotoPost)posts.get(index)).getPhotoUrl().get(maxWidth);
    }

    @Override
    public String getQuotePostDetails(int index) {
        return ((QuotePost)posts.get(index)).getQuoteSource();
    }

    @Override
    public String getRegularPostDetails(int index) {
        return ((RegularPost)posts.get(index)).getRegularBody();
    }

    @Override
    public ConversationPost getConversationPost(int index) {
        return (ConversationPost)posts.get(index);
    }

}
