package com.n4zapps.tumblrbrowser.repositories.dataobjects;

import java.util.List;

public abstract class BasePost {

    public static final String TYPE_REGULAR         = "regular";
    public static final String TYPE_TEXT            = "text";
    public static final String TYPE_QUOTE           = "quote";
    public static final String TYPE_PHOTO           = "photo";
    public static final String TYPE_LINK            = "link";
    public static final String TYPE_CHAT            = "chat";
    public static final String TYPE_CONVERSATION    = "conversation";
    public static final String TYPE_VIDEO           = "video";
    public static final String TYPE_AUDIO           = "audio";
    public static final String TYPE_ANSWER          = "answer";

    private String id;
    private String url;
    private long timestamp;
    private String type;
    private List<String> tags;

    public BasePost(String id, String url, long timestamp, String type, List<String> tags) {
        this.id = id;
        this.url = url;
        this.timestamp = timestamp;
        this.type = type;
        this.tags = tags;
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getType() {
        return type;
    }

    public List<String> getTags() {
        return tags;
    }

}
