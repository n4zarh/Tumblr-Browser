package com.n4zapps.tumblrbrowser.repositories.dataobjects;

import java.util.List;
import java.util.Map;

public class PhotoPost extends BasePost {

    public static final int MAX_SIZE_1280 = 1280;
    public static final int MAX_SIZE_500 = 500;
    public static final int MAX_SIZE_400 = 400;
    public static final int MAX_SIZE_250 = 250;
    public static final int MAX_SIZE_100 = 100;
    public static final int MAX_SIZE_75 = 75;

    private String photoCaption;
    private Map<Integer, String> photoUrl;

    public PhotoPost(String id, String url, long timestamp, String type, List<String> tags, String photoCaption, Map<Integer, String> photoUrl) {
        super(id, url, timestamp, type, tags);
        this.photoCaption = photoCaption;
        this.photoUrl = photoUrl;
    }

    public String getPhotoCaption() {
        return photoCaption;
    }

    public Map<Integer, String> getPhotoUrl() {
        return photoUrl;
    }

}
