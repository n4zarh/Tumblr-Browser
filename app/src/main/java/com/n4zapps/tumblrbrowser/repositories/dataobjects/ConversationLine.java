package com.n4zapps.tumblrbrowser.repositories.dataobjects;

public class ConversationLine {

    private String name;
    private String label;
    private String phrase;

    public ConversationLine(String name, String label, String phrase) {
        this.name = name;
        this.label = label;
        this.phrase = phrase;
    }

    public String getName() {
        return name;
    }

    public String getLabel() {
        return label;
    }

    public String getPhrase() {
        return phrase;
    }

}
