package com.n4zapps.tumblrbrowser.repositories.dataobjects;

import java.util.List;

public class RegularPost extends BasePost {

    private String regularTitle;
    private String regularBody;

    public RegularPost(String id, String url, long timestamp, String type, List<String> tags, String regularTitle, String regularBody) {
        super(id, url, timestamp, type, tags);
        this.regularTitle = regularTitle;
        this.regularBody = regularBody;
    }

    public String getRegularTitle() {
        return regularTitle;
    }

    public String getRegularBody() {
        return regularBody;
    }
}
