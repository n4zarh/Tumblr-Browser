package com.n4zapps.tumblrbrowser.repositories.dataobjects;

import java.util.List;

public class AudioPost extends BasePost {

    private String audioPlayer;
    private String audioCaption;

    public AudioPost(String id, String url, long timestamp, String type, List<String> tags, String audioPlayer, String audioCaption) {
        super(id, url, timestamp, type, tags);
        this.audioPlayer = audioPlayer;
        this.audioCaption = audioCaption;
    }

    public String getAudioPlayer() {
        return audioPlayer;
    }

    public String getAudioCaption() {
        return audioCaption;
    }

}
