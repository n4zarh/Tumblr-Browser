package com.n4zapps.tumblrbrowser.repositories.dataobjects;

import java.util.List;

public class ConversationPost extends BasePost {

    private String conversationTitle;
    private String conversationText;
    private List<ConversationLine> conversation;

    public ConversationPost(String id, String url, long timestamp, String type, List<String> tags, String conversationTitle, String conversationText, List<ConversationLine> conversation) {
        super(id, url, timestamp, type, tags);
        this.conversationTitle = conversationTitle;
        this.conversationText = conversationText;
        this.conversation = conversation;
    }

    public String getConversationTitle() {
        return conversationTitle;
    }

    public String getConversationText() {
        return conversationText;
    }

    public List<ConversationLine> getConversation() {
        return conversation;
    }

}
