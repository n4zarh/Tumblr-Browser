package com.n4zapps.tumblrbrowser.repositories.dataobjects;

import java.util.List;

public class VideoPost extends BasePost {

    private String videoCaption;
    private String videoSource;
    private String videoPlayer;

    public VideoPost(String id, String url, long timestamp, String type, List<String> tags, String videoCaption, String videoSource, String videoPlayer) {
        super(id, url, timestamp, type, tags);
        this.videoCaption = videoCaption;
        this.videoSource = videoSource;
        this.videoPlayer = videoPlayer;
    }

    public String getVideoCaption() {
        return videoCaption;
    }

    public String getVideoSource() {
        return videoSource;
    }

    public String getVideoPlayer() {
        return videoPlayer;
    }

}
