package com.n4zapps.tumblrbrowser.repositories.dataobjects;

import java.util.List;

public class QuotePost extends BasePost {

    private String quoteText;
    private String quoteSource;

    public QuotePost(String id, String url, long timestamp, String type, List<String> tags, String quoteText, String quoteSource) {
        super(id, url, timestamp, type, tags);
        this.quoteText = quoteText;
        this.quoteSource = quoteSource;
    }

    public String getQuoteText() {
        return quoteText;
    }

    public String getQuoteSource() {
        return quoteSource;
    }

}
