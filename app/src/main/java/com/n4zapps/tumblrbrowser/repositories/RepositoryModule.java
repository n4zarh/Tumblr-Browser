package com.n4zapps.tumblrbrowser.repositories;

import com.n4zapps.tumblrbrowser.interfaces.PostRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    // Zabrakło mi czasu na przeróbkę oraz zwracanie singletona zamiast pola statycznego

    private static PostRepository postRepository;

    @Provides
    public PostRepository providePostRepository() {
        if (postRepository == null) {
            postRepository = new MemoryPostRepository();
        }
        return postRepository;
    }

}
