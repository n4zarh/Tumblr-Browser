package com.n4zapps.tumblrbrowser.repositories.dataobjects;

import java.util.List;

public class AnswerPost extends BasePost {

    private String question;
    private String answer;

    public AnswerPost(String id, String url, long timestamp, String type, List<String> tags, String question, String answer) {
        super(id, url, timestamp, type, tags);
        this.question = question;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

}
