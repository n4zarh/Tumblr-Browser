package com.n4zapps.tumblrbrowser.root;

import android.app.Application;
import android.content.Context;

import dagger.Module;
import dagger.Provides;


@Module
public class ApplicationModule {

    @Provides
    public Context provideContext(Application application) {
        return application;
    }

}
