package com.n4zapps.tumblrbrowser.root;

import com.n4zapps.tumblrbrowser.views.ChooseNicknameActivity;
import com.n4zapps.tumblrbrowser.views.ChooseNicknameActivityModule;
import com.n4zapps.tumblrbrowser.views.ShowPostsActivity;
import com.n4zapps.tumblrbrowser.views.ShowPostsActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = ChooseNicknameActivityModule.class)
    abstract ChooseNicknameActivity bindChooseNicknameActivity();

    @ContributesAndroidInjector(modules = ShowPostsActivityModule.class)
    abstract ShowPostsActivity bindShowPostsActivity();

}
