package com.n4zapps.tumblrbrowser.root;

import android.app.Activity;
import android.app.Application;

import com.n4zapps.tumblrbrowser.interfaces.ApplicationComponent;
import com.n4zapps.tumblrbrowser.interfaces.DaggerApplicationComponent;
import com.n4zapps.tumblrbrowser.repositories.RepositoryModule;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

public class MyApplication extends Application
                            implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;

    static RepositoryModule repositoryModule;
    static ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        repositoryModule = new RepositoryModule();
        component = DaggerApplicationComponent.builder()
                    .application(this)
                    .repositoryModule(repositoryModule)
                    .build();
        component.inject(this);
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }

    public static ApplicationComponent getComponent() {
        return component;
    }

    public static RepositoryModule getRepositoryModule() {
        return repositoryModule;
    }
}
