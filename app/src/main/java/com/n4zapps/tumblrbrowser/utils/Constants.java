package com.n4zapps.tumblrbrowser.utils;

public class Constants {

    public static final int TIMEOUT_DURATION   = 10000;
    public static final int TIMEOUT_COUNT      = 3;

    public static final String BASE_URL_PRE_NAME    = "https://";
    public static final String BASE_URL_POST_NAME   = ".tumblr.com/api/read/json";

    public static final int NUMBER_OF_POSTS_PER_QUERY       = 30;
    public static final int POST_MOD_TO_DOWNLOAD_NEW_DATA   = 20;

}
