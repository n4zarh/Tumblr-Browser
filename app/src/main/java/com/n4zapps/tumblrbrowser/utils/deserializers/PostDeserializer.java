package com.n4zapps.tumblrbrowser.utils.deserializers;

import android.annotation.SuppressLint;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.AnswerPost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.AudioPost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.BasePost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.ConversationLine;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.ConversationPost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.LinkPost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.PhotoPost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.QuotePost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.RegularPost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.VideoPost;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@SuppressLint("UseSparseArrays")
public class PostDeserializer implements JsonDeserializer<BasePost> {
    @Override
    public BasePost deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject base = json.getAsJsonObject();

        String id = base.get("id").getAsString();
        String url = base.get("url").getAsString();
        long timestamp = base.get("unix-timestamp").getAsLong();
        String type = base.get("type").getAsString();
        JsonElement tagsArray = base.get("tags");
        List<String> tags = new ArrayList<>();

        if (tagsArray != null) {
            JsonArray array = tagsArray.getAsJsonArray();
            for (int i=0; i<array.size(); i++) {
                tags.add(array.get(i).getAsString());
            }
        }

        switch (type) {
            case BasePost.TYPE_ANSWER:
                String question = decodeJsonElement(base, "question");
                String answer = decodeJsonElement(base, "answer");
                return new AnswerPost(id, url, timestamp, type, tags, question, answer);
            case BasePost.TYPE_AUDIO:
                String audioCaption = decodeJsonElement(base, "audio-caption");
                String audioPlayer = decodeJsonElement(base, "audio-player");
                return new AudioPost(id, url, timestamp, type, tags, audioPlayer, audioCaption);
            case BasePost.TYPE_CHAT:
            case BasePost.TYPE_CONVERSATION:
                String conversationTitle = decodeJsonElement(base, "conversation-title");
                String conversationText = decodeJsonElement(base, "conversation-text");
                List<ConversationLine> conversation = new ArrayList<>();
                JsonElement conversationArray = base.get("conversation");
                if (conversationArray != null) {
                    JsonArray array = conversationArray.getAsJsonArray();
                    for (int i=0; i<array.size(); i++) {
                        JsonObject subbase = array.get(i).getAsJsonObject();
                        String name = decodeJsonElement(subbase, "name");
                        String label = decodeJsonElement(subbase, "label");
                        String phrase = decodeJsonElement(subbase, "phrase");
                        conversation.add(new ConversationLine(name, label, phrase));
                    }
                }
                return new ConversationPost(id, url, timestamp, type, tags, conversationTitle, conversationText, conversation);
            case BasePost.TYPE_LINK:
                String linkText = decodeJsonElement(base, "link-text");
                String linkUrl = decodeJsonElement(base, "link-url");
                return new LinkPost(id, url, timestamp, type, tags, linkText, linkUrl);
            case BasePost.TYPE_PHOTO:
                String photoCaption = decodeJsonElement(base, "photo-caption");
                Map<Integer, String> photoUrl = new HashMap<>();
                Integer[] maxWidths = new Integer[]{1280, 500, 400, 250, 100, 75};
                for (Integer width : maxWidths) {
                    photoUrl.put(width, decodeJsonElement(base, "photo-url-" + String.valueOf(width)));
                }
                return new PhotoPost(id, url, timestamp, type, tags, photoCaption, photoUrl);
            case BasePost.TYPE_QUOTE:
                String quoteText = decodeJsonElement(base, "quote-text");
                String quoteSource = decodeJsonElement(base, "quote-source");
                return new QuotePost(id, url, timestamp, type, tags, quoteText, quoteSource);
            case BasePost.TYPE_REGULAR:
            case BasePost.TYPE_TEXT:
                String regularTitle = decodeJsonElement(base, "regular-title");
                String regularBody = decodeJsonElement(base, "regular-body");
                return new RegularPost(id, url, timestamp, type, tags, regularTitle, regularBody);
            case BasePost.TYPE_VIDEO:
                String videoCaption = decodeJsonElement(base, "video-caption");
                String videoSource = decodeJsonElement(base, "video-source");
                String videoPlayer = decodeJsonElement(base, "video-player");
                return new VideoPost(id, url, timestamp, type, tags, videoCaption, videoSource, videoPlayer);
            default:
                return null;
        }

    }

    private static String decodeJsonElement(JsonObject base, String toDecode) {
        JsonElement element = base.get(toDecode);
        return element.isJsonNull() ? null : element.getAsString();
    }

}
