package com.n4zapps.tumblrbrowser.utils.requests;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public class RequestManager {

    private RequestQueue queue;

    private ImageLoader imageLoader;

    public RequestManager(Context context) {
        try {
            queue = Volley.newRequestQueue(context);
            imageLoader = new ImageLoader(queue, new ImageLoader.ImageCache() {
                private final LruCache<String, Bitmap> cache = new LruCache<>(10);
                @Override
                public Bitmap getBitmap(String url) {
                    return cache.get(url);
                }

                @Override
                public void putBitmap(String url, Bitmap bitmap) {
                    cache.put(url, bitmap);
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    public void addRequestToQueue(Request request) {
        PrintCurlLog.printLog(request);
        queue.add(request);
    }

    public void cancelAllRequests() {
        queue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });
    }

}
