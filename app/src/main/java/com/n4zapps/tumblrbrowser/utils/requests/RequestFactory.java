package com.n4zapps.tumblrbrowser.utils.requests;

import com.android.volley.Response;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.TumblrNetworkResponse;

public class RequestFactory {

    public ObjectRequest createPostRequest(String username, String type, Response.Listener<TumblrNetworkResponse> listener, Response.ErrorListener errorListener) {
        return new GetPostsRequest(username, type, listener, errorListener);
    }

    public ObjectRequest createPostRequestWithPage(String username, int page, String type, Response.Listener<TumblrNetworkResponse> listener, Response.ErrorListener errorListener) {
        return new GetPostsRequest(username, page, type, listener, errorListener);
    }

}
