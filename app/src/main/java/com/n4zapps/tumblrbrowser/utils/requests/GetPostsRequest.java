package com.n4zapps.tumblrbrowser.utils.requests;

import com.android.volley.Response;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.TumblrNetworkResponse;
import com.n4zapps.tumblrbrowser.utils.Constants;

import java.util.HashMap;
import java.util.Map;

class GetPostsRequest extends ObjectRequest<TumblrNetworkResponse> {

    GetPostsRequest(String username, String type, Response.Listener<TumblrNetworkResponse> listener, Response.ErrorListener errorListener) {
        super(Method.GET, createUrl(username), TumblrNetworkResponse.class, createParams(0, type), listener, errorListener);
    }

    GetPostsRequest(String username, int page, String type, Response.Listener<TumblrNetworkResponse> listener, Response.ErrorListener errorListener) {
        super(Method.GET, createUrl(username), TumblrNetworkResponse.class, createParams(page, type), listener, errorListener);
    }

    private static String createUrl(String username) {
        return Constants.BASE_URL_PRE_NAME + username + Constants.BASE_URL_POST_NAME;
    }

    private static Map<String, String> createParams(int page, String type) {
        Map<String, String> params = new HashMap<>();
        params.put("num", String.valueOf(Constants.NUMBER_OF_POSTS_PER_QUERY));
        params.put("start", String.valueOf(page * Constants.NUMBER_OF_POSTS_PER_QUERY));
        if (type != null) {
            params.put("type", type);
        }
        params.put("filter", "text");
        return params;
    }

}
