package com.n4zapps.tumblrbrowser.utils.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.n4zapps.tumblrbrowser.utils.Logger;

public class PrintCurlLog {

    public static void printLog(Request<?> request) {
        StringBuilder builder = new StringBuilder();
        builder.append("curl request: curl ")
                .append("-X \"");
        switch (request.getMethod()) {
            case Request.Method.GET:
                builder.append("GET");
                break;
            case Request.Method.POST:
                builder.append("POST");
                break;
            case Request.Method.PUT:
                builder.append("PUT");
                break;
            case Request.Method.DELETE:
                builder.append("DELETE");
                break;
        }
        builder.append("\"");
        Logger.d("VOLLEY", builder.toString());
        builder.setLength(0);
        try {
            if (request.getBody() != null) {
                builder.append(" -D ");
                String data = new String(request.getBody());
                data = data.replaceAll("\"", "\\\"");
                builder.append("\"");
                builder.append(data);
                builder.append("\"");
                Logger.d("VOLLEY", builder.toString());
                builder.setLength(0);
            }
            for (String key : request.getHeaders().keySet()) {
                builder.append(" -H ")
                        .append(key)
                        .append(": ")
                        .append(request.getHeaders().get(key));
                Logger.d("VOLLEY", builder.toString());
                builder.setLength(0);
            }
            builder.append(" \"");
            builder.append(request.getUrl());
            builder.append("\"");
            Logger.d("VOLLEY", builder.toString());
        } catch (AuthFailureError e) {
            VolleyLog.wtf("Unable to get body of response or headers for curl logging");
        }
    }

}
