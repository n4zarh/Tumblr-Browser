package com.n4zapps.tumblrbrowser.utils.requests;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.BasePost;
import com.n4zapps.tumblrbrowser.utils.Constants;
import com.n4zapps.tumblrbrowser.utils.Logger;
import com.n4zapps.tumblrbrowser.utils.deserializers.PostDeserializer;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

public class ObjectRequest<T> extends Request<T> {

    private final Gson gson;
    private final Class<T> jsonClass;
    private final Response.Listener<T> listener;

    ObjectRequest(int method,
                  String url,
                  Class<T> jsonClass,
                  Map<String, String> params,
                  Response.Listener<T> listener,
                  Response.ErrorListener errorListener) {
        super(method, createUrlWithParams(url, params), errorListener);
        this.listener = listener;
        this.jsonClass = jsonClass;
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(BasePost.class, new PostDeserializer());
        builder.setLenient();
        gson = builder.create();
        setRetryPolicy(new DefaultRetryPolicy(Constants.TIMEOUT_DURATION,
                                                Constants.TIMEOUT_COUNT,
                                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private static String createUrlWithParams(String url, Map<String, String> params) {
        StringBuilder builder = new StringBuilder(url);
        if (params != null) {
            boolean first = true;
            for (Map.Entry<String, String> entry : params.entrySet()) {
                if (first) {
                    builder.append("?");
                    first = false;
                } else {
                    builder.append("&");
                }
                try {
                    builder.append(entry.getKey())
                            .append("=")
                            .append(entry.getValue() == null ? "" : URLEncoder.encode(entry.getValue(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        return builder.toString();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            Logger.d("RAW RESPONSE", new String(response.data, HttpHeaderParser.parseCharset(response.headers)));
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            String json2 = json.substring(json.indexOf("{")); //Removing "var tumblr_api_read = " from the beggining, leaving only a json object
            String json3 = json2.substring(0, json2.length() - 2); //removing semicolon from the end
            T result = gson.fromJson(json3, jsonClass);
            return Response.success(result, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    public void deliverError(VolleyError error) {
        super.deliverError(error);
    }

}
