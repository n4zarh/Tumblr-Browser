package com.n4zapps.tumblrbrowser.interfaces;

import com.n4zapps.tumblrbrowser.repositories.RepositoryModule;
import com.n4zapps.tumblrbrowser.views.viewholders.ConversationViewHolder;
import com.n4zapps.tumblrbrowser.views.viewholders.ConversationViewHolderModule;

import dagger.Component;

@PerViewHolder
@Component(dependencies = {ApplicationComponent.class},
            modules = {ConversationViewHolderModule.class, RepositoryModule.class})
public interface ConversationViewHolderComponent {

    void inject(ConversationViewHolder viewHolder);

}
