package com.n4zapps.tumblrbrowser.interfaces;

public interface SingleLineCellView {

    void setTextLine(String name, String message, boolean setGrayBackground);

}
