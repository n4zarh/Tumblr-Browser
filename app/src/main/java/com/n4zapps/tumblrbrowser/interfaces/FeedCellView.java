package com.n4zapps.tumblrbrowser.interfaces;

import com.android.volley.toolbox.ImageLoader;

public interface FeedCellView {

    void setNameDateAndType(String name, String date, String type);

    interface RegularCellView extends FeedCellView {
        void setDetails(String details);
        void setDetailsGravityToRight();
    }

    interface PhotoCellView extends FeedCellView {
        void setPhoto(String url, ImageLoader imageLoader);
    }

    interface ConversationCellView extends FeedCellView {
        void setConversationPresenter(int position);
    }

}
