package com.n4zapps.tumblrbrowser.interfaces;

import com.android.volley.toolbox.ImageLoader;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.ConversationLine;
import com.n4zapps.tumblrbrowser.views.viewholders.ConversationViewHolder;
import com.n4zapps.tumblrbrowser.views.viewholders.PhotoViewHolder;
import com.n4zapps.tumblrbrowser.views.viewholders.PostViewHolder;
import com.n4zapps.tumblrbrowser.views.viewholders.SingleLineViewHolder;

import java.util.List;

public interface ShowPostsActivityMVP {

    interface View {
        void notifyAdapterAboutNewItems(int firstAddedAtIndex, int numberOfItems);
        String getStringFromResources(int resId);
    }

    interface Model {
        void downloadData(String username, String type, int page, OnDataDownloadedListener listener);
        int getItemCount();
        String getItemName(int position);
        String getItemType(int position);
        String getItemDate(int position);
        String getItemDetails(int position);
        String getPhotoUrl(int position);
        ImageLoader getImageLoader();
        String getItemUri(int position);

        interface OnDataDownloadedListener {
            void onDataLoaded(int count);
        }
    }

    interface Presenter {
        void setUsername(String username);
        void setType(String type);
        int getItemCount();
        void downloadNextPage();
        String getItemType(int position);
        void onBindRegularCellViewAtPosition(int position, PostViewHolder viewHolder);
        void onBindPhotoCellViewAtPosition(int position, PhotoViewHolder viewHolder);
        void onBindConversationViewAtPosition(int position, ConversationViewHolder viewHolder);
        String getItemUri(int position);
    }

    interface SingleLineModel {
        List<ConversationLine> getConversation(int position);
    }

    interface SingleLinePresenter {
        void onBindSingleLineViewAtPosition(int position, SingleLineViewHolder viewHolder);
        void setNewPosition(int position);
        int getItemCount();
    }

}
