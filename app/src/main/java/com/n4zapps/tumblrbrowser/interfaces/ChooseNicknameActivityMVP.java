package com.n4zapps.tumblrbrowser.interfaces;

/**
 * Created by Naziorkowski on 05.05.2018.
 */

public interface ChooseNicknameActivityMVP {

    interface View {
        String getUsername();
        void showNoUsernameErrorToast();
        void showNoUserFoundErrorToast();
        void moveToShowDataActivity(String username, String type);
        void showNoConnectionErrorToast();
        void showEmptyFeedError();
        int getCheckedTypeId();
    }

    interface Presenter {
        void onGetDataButtonClicked();
    }

    interface Model {
        void downloadData(String username, String type, OnDataDownloadedListener onDataDownloadedListener);

        interface OnDataDownloadedListener {
            void onDataLoaded(int count);
            void usernameNotFound();
            void noConnectionError();
            void emptyFeed();
        }
    }

}
