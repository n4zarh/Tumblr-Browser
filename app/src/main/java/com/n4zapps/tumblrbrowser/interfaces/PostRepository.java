package com.n4zapps.tumblrbrowser.interfaces;

import com.n4zapps.tumblrbrowser.repositories.dataobjects.BasePost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.ConversationPost;

public interface PostRepository {

    void populateList(BasePost[] objects);
    void clearList();

    int getPostCount();

    String getPostType(int index);
    String getPostText(int index);
    String getPostTime(int index);

    String getPostUri(int index);

    String getAnswerPostDetails(int index);
    String getLinkPostDetails(int index);
    String getPhotoPostLink(int index, int maxWidth);
    String getQuotePostDetails(int index);
    String getRegularPostDetails(int index);

    ConversationPost getConversationPost(int index);

}
