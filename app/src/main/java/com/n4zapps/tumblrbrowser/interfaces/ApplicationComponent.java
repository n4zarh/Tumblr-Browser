package com.n4zapps.tumblrbrowser.interfaces;


import android.app.Application;

import com.n4zapps.tumblrbrowser.repositories.RepositoryModule;
import com.n4zapps.tumblrbrowser.root.ActivityBuilder;
import com.n4zapps.tumblrbrowser.root.ApplicationModule;
import com.n4zapps.tumblrbrowser.root.MyApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        ApplicationModule.class,
        ActivityBuilder.class,
        RepositoryModule.class})
public interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance Builder application(Application application);
        @BindsInstance Builder repositoryModule(RepositoryModule repositoryModule);
        ApplicationComponent build();
    }

    void inject(MyApplication app);

}
