package com.n4zapps.tumblrbrowser.model;

import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.n4zapps.tumblrbrowser.interfaces.ChooseNicknameActivityMVP;
import com.n4zapps.tumblrbrowser.interfaces.PostRepository;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.TumblrNetworkResponse;
import com.n4zapps.tumblrbrowser.utils.Logger;
import com.n4zapps.tumblrbrowser.utils.requests.RequestFactory;
import com.n4zapps.tumblrbrowser.utils.requests.RequestManager;

import java.util.Arrays;

import javax.inject.Inject;

public class ChooseNicknameModel implements ChooseNicknameActivityMVP.Model {

    private RequestManager requestManager;
    private PostRepository repository;

    @Inject
    public ChooseNicknameModel(RequestManager requestManager, PostRepository repository) {
        this.repository = repository;
        this.requestManager = requestManager;
    }

    @Override
    public void downloadData(String username, String type, final OnDataDownloadedListener onDataDownloadedListener) {
        repository.clearList();
        RequestFactory factory = new RequestFactory();
        requestManager.addRequestToQueue(factory.createPostRequest(username, type, new Response.Listener<TumblrNetworkResponse>() {
            @Override
            public void onResponse(TumblrNetworkResponse response) {
                Logger.d("DATA", Arrays.toString(response.getPosts()));
                if (response.getPosts().length == 0) {
                    onDataDownloadedListener.emptyFeed();
                } else {
                    repository.populateList(response.getPosts());
                    onDataDownloadedListener.onDataLoaded(response.getPosts().length);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Logger.e("DATA", error.toString());
                if (error instanceof NoConnectionError) {
                    onDataDownloadedListener.noConnectionError();
                } else {
                    if (error.networkResponse != null && error.networkResponse.statusCode == 404) {
                        onDataDownloadedListener.usernameNotFound();
                    }
                }
            }
        }));
    }

}
