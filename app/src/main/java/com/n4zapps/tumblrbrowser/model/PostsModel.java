package com.n4zapps.tumblrbrowser.model;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.n4zapps.tumblrbrowser.interfaces.PostRepository;
import com.n4zapps.tumblrbrowser.interfaces.ShowPostsActivityMVP;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.BasePost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.PhotoPost;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.TumblrNetworkResponse;
import com.n4zapps.tumblrbrowser.utils.Logger;
import com.n4zapps.tumblrbrowser.utils.requests.RequestFactory;
import com.n4zapps.tumblrbrowser.utils.requests.RequestManager;

import java.util.Arrays;

import javax.inject.Inject;

public class PostsModel implements ShowPostsActivityMVP.Model {

    private RequestManager requestManager;
    private PostRepository repository;

    @Inject
    public PostsModel(RequestManager requestManager,
                      PostRepository repository) {
        this.requestManager = requestManager;
        this.repository = repository;
    }

    @Override
    public void downloadData(String username, String type, int page, final OnDataDownloadedListener listener) {
        RequestFactory factory = new RequestFactory();
        requestManager.addRequestToQueue(factory.createPostRequestWithPage(username, page, type, new Response.Listener<TumblrNetworkResponse>() {
            @Override
            public void onResponse(TumblrNetworkResponse response) {
                Logger.d("DATA", Arrays.toString(response.getPosts()));
                repository.populateList(response.getPosts());
                listener.onDataLoaded(response.getPosts().length);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Logger.e("DATA", error.toString());
            }
        }));
    }

    @Override
    public int getItemCount() {
        return repository.getPostCount();
    }

    @Override
    public String getItemName(int position) {
        return repository.getPostText(position);
    }

    @Override
    public String getItemType(int position) {
        return repository.getPostType(position);
    }

    @Override
    public String getItemDate(int position) {
        return repository.getPostTime(position);
    }

    @Override
    public String getItemDetails(int position) {
        switch (repository.getPostType(position)) {
            case BasePost.TYPE_ANSWER:
                return repository.getAnswerPostDetails(position);
            case BasePost.TYPE_AUDIO:
                return null;
            case BasePost.TYPE_LINK:
                return repository.getLinkPostDetails(position);
            case BasePost.TYPE_QUOTE:
                return repository.getQuotePostDetails(position);
            case BasePost.TYPE_REGULAR:
            case BasePost.TYPE_TEXT:
                return repository.getRegularPostDetails(position);
            case BasePost.TYPE_VIDEO:
                return null;
        }
        return null;
    }

    @Override
    public String getPhotoUrl(int position) {
        if (repository.getPostType(position).equals(BasePost.TYPE_PHOTO)) {
            return repository.getPhotoPostLink(position, PhotoPost.MAX_SIZE_1280);
        }
        return null;
    }

    @Override
    public ImageLoader getImageLoader() {
        return requestManager.getImageLoader();
    }

    @Override
    public String getItemUri(int position) {
        return repository.getPostUri(position);
    }

}
