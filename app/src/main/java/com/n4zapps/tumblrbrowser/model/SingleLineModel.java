package com.n4zapps.tumblrbrowser.model;

import com.n4zapps.tumblrbrowser.interfaces.PostRepository;
import com.n4zapps.tumblrbrowser.interfaces.ShowPostsActivityMVP;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.ConversationLine;
import com.n4zapps.tumblrbrowser.repositories.dataobjects.ConversationPost;

import java.util.List;

import javax.inject.Inject;

public class SingleLineModel implements ShowPostsActivityMVP.SingleLineModel {

    private PostRepository postRepository;

    @Inject
    public SingleLineModel(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public List<ConversationLine> getConversation(int position) {
        ConversationPost post = postRepository.getConversationPost(position);
        return post == null ? null : post.getConversation();
    }

}
